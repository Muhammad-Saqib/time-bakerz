(function(){

  angular
       .module('users')
       .controller('UserController', [
         '$scope', 'userService', '$mdSidenav', '$mdBottomSheet', '$log', '$interval',
          UserController
       ])
      .filter('keyboardShortcut',keyboardShortcut);

  /**
   * Main Controller for the Angular Material Starter App
   * @param $scope
   * @param $mdSidenav
   * @param avatarsService
   * @constructor
   */
  function UserController( $scope, userService, $mdSidenav, $mdBottomSheet, $log, $interval) {

      this.currentNotification = 0;
      this.fadePara = this.currentNotification;
      this.notifications = [
          {data:'Lorem Ipsum. Proin gravida nibh vel velit auctor' +
          ' aliquet. Aenean sollicitudin, lorem quis bibendum' +
          ' auctor, nisi elit consequat .', quote: 'Proin gravida nibh vel velit auctor aliquet. Aenean' +
          ' sollicitudin, lorem quis bibendum auctor, nisi elit.', title:'Wordpress'},
          {data:'Lorem Ipsum. Proin gravida nibh vel velit auctor' +
          ' aliquet. Aenean sollicitudin, lorem quis bibendum' +
          ' auctor, nisi elit consequat ipsum elit ipsum elit.', quote: 'Lorem Ipsum.Proin gravida nibh vel velit auctor aliquet. Aenean' +
              ' sollicitudin, lorem quis bibendum auctor, nisi elit.', title:'php'},
          {data:'Lorem Ipsum. Proin gravida nibh vel velit auctor' +
          ' aliquet. Aenean sollicitudin, lorem quis bibendum' +
          ' ipsum elit.', quote: 'Proin gravida nibh vel velit auctor aliquet. Aenean' +
          ' sollicitudin, lorem quis bibendum auctor, nisi elit.', title:'jQuery'},
          {data:'Lorem Ipsum. Proin gravida nibh vel velit auctor' +
          ' aliquet. Aenean sollicitudin, lorem quis bibendum' +
          ' auctor, nisi elit consequat ipsum elit auctor, nisi elit.', quote: 'Proin gravida nibh vel velit aliquet. Aenean' +
          ' sollicitudin, lorem quis bibendum auctor, nisi elit.', title:'github'},
          {data:'Lorem Ipsum. Proin gravida nibh vel velit auctor' +
          ' aliquet. Aenean sollicitudin, lorem quis bibendum' +
          ' auctor, nisi elit consequat ipsum elit Proin gravida nibh vel velit .', quote: 'Proin gravida nibh vel velit auctor aliquet. Aenean' +
          ' sollicitudin, lorem quis bibendum auctor, nisi elit.', title:'GRUNT'},
          {data:'Lorem Ipsum. Proin gravida nibh vel velit auctor' +
          ' aliquet. Aenean sollicitudin, lorem quis bibendum' +
          ' auctor, nisi elit consequat ipsum elit.', quote: 'Proin gravida nibh vel velit auctor aliquet. Aenean' +
          ' sollicitudin, lorem quis bibendum auctor, nisi elit.', title:'jsHint'},
          {data:'Lorem Ipsum. Proin gravida nibh vel velit auctor' +
          ' aliquet. Aenean sollicitudin, lorem quis bibendum' +
          ' auctor, nisi elit consequat ipsum elit gravida nibh vel velit.', quote: 'Proin gravida nibh vel velit auctor aliquet. Aenean' +
          ' sollicitudin, lorem quis bibendum auctor, nisi elit.', title:'nodejs'},
          {data:'Lorem Ipsum. Proin gravida nibh vel velit auctor' +
          ' aliquet. Aenean sollicitudin, lorem quis bibendum' +
          ' auctor, nisi elit consequat ipsum elit gravida nibh vel velit.', quote: 'Proin gravida nibh vel velit auctor aliquet. Aenean' +
          ' sollicitudin, lorem quis bibendum auctor, nisi elit.', title:'envato'}
      ];

      //var temp = 1;
      //$interval(function() {
      //    console.log('click event');
      //    angular.element('#foo' + temp).trigger('click');
      //    temp++;
      //    if(temp > 9) temp = 1;
      //}, 5000);

      $scope.demo = {
          tipDirection : 'top'
      };
      $scope.$watch('demo.tipDirection',function(val) {
          if (val && val.length ) {
              $scope.demo.showTooltip = true;
          }
      })
      angular.element('.scroll').newsTicker({
          row_height: 250,
          max_rows: 3,
          speed: 1200,
          direction: 'up',
          duration: 5000,
          autostart: 5,
          pauseOnHover: 0
      });

      this.isOpen = true;
      this.demo = {
          isOpen: true,
          count: 0,
          selectedDirection: 'left'
      };

    var self = this;

    self.selected     = null;
    self.users        = [ ];
    self.selectUser   = selectUser;
    self.toggleList   = toggleUsersList;
    self.makeContact  = makeContact;

    // Load all registered users

    userService
          .loadAllUsers()
          .then( function( users ) {
            self.users    = [].concat(users);
            self.selected = users[0];
          });

    // *********************************
    // Internal methods
    // *********************************

    /**
     * Hide or Show the 'left' sideNav area
     */
    function toggleUsersList() {
      $mdSidenav('left').toggle();
    }

    /**
     * Select the current avatars
     * @param menuId
     */
    function selectUser ( user ) {
      self.selected = angular.isNumber(user) ? $scope.users[user] : user;
    }

    /**
     * Show the Contact view in the bottom sheet
     */
    function makeContact(selectedUser) {

        $mdBottomSheet.show({
          controllerAs  : "cp",
          templateUrl   : './src/users/view/contactSheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });

        /**
         * User ContactSheet controller
         */
        function ContactSheetController( $mdBottomSheet ) {
          this.user = selectedUser;
          this.actions = [
            { name: 'Phone'       , icon: 'phone'       , icon_url: 'assets/svg/phone.svg'},
            { name: 'Twitter'     , icon: 'twitter'     , icon_url: 'assets/svg/twitter.svg'},
            { name: 'Google+'     , icon: 'google_plus' , icon_url: 'assets/svg/google_plus.svg'},
            { name: 'Hangout'     , icon: 'hangouts'    , icon_url: 'assets/svg/hangouts.svg'}
          ];
          this.contactUser = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
        }
    }

  }

    function  keyboardShortcut($window) {
        return function(str) {
            if (!str) return;
            var keys = str.split('-');
            var isOSX = /Mac OS X/.test($window.navigator.userAgent);
            var seperator = (!isOSX || keys.length > 2) ? '+' : '';
            var abbreviations = {
                M: isOSX ? '?' : 'Ctrl',
                A: isOSX ? 'Option' : 'Alt',
                S: 'Shift'
            };
            return keys.map(function(key, index) {
                var last = index == keys.length - 1;
                return last ? key : abbreviations[key];
            }).join(seperator);
        };
    }

})();
